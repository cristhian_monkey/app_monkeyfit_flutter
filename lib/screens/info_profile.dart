import 'package:flutter/material.dart';
class infoProfile extends StatelessWidget {

  String nameText;
  String valueDate;

  infoProfile(this.nameText, this.valueDate);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          nameText,
          style: TextStyle(
            color:
                Color.fromRGBO(6, 21, 255, 1.0),
            fontFamily: 'stolzl_displaybold',
            fontWeight: FontWeight.bold,
            fontSize: 15.0,
          )
        ),
        Text(
        valueDate,
          style: TextStyle(
            color:
                Color.fromRGBO(82, 82, 83, 1.0),
            fontFamily: 'GTCinetypeRegular',
            fontWeight: FontWeight.bold,
            fontSize: 15.0,
          )
        )
      ]
    );
  }
}