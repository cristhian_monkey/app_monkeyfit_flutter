import 'package:flutter/material.dart'
    show
        AppBar,
        BuildContext,
        Color,
        Colors,
        Key,
        Scaffold,
        State,
        StatefulWidget,
        Text,
        TextStyle,
        Widget;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class NewViewCenter extends StatefulWidget {
  const NewViewCenter({Key key, this.token, this.urlName}) : super(key: key);
  final String token;
  final String urlName;

  @override
  _MonkeyAppState createState() => _MonkeyAppState();
}

class _MonkeyAppState extends State<NewViewCenter> {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 21, 255, 1.0),
        title: Text('${widget.urlName.toUpperCase()}',
            style: TextStyle(
                color: Colors.white, fontFamily: 'stolzl_displaybold')),
      ),
      body: WebviewScaffold(
        debuggingEnabled: true,
        withZoom: true,
        withJavascript: true,
        hidden: true,
        url:
            'https://www.monkeyfitpass.com/${widget.urlName}?webview_token=${widget.token}&showNavApp=0',
      ),
    );
  }
}
