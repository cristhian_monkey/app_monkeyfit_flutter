import 'dart:async' show Future;

import 'package:Monkeyfitpass/screens/homeCenters.dart';
import 'package:Monkeyfitpass/screens/loaderIntro.dart';
import 'package:Monkeyfitpass/screens/loginScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key, this.cookie}) : super(key: key);
  final String cookie;

  @override
  _MonkeyAppState createState() => _MonkeyAppState();
}

class _MonkeyAppState extends State<HomeScreen> {
  Future<void> future;
  String token = '';
  int nToken = 0;

  Future<void> readToken() async {
    var twoSecondsFuture = Future.delayed(Duration(seconds: 3));
    var prefs = await SharedPreferences.getInstance();
    token = prefs.getString("token");
    await twoSecondsFuture;
    setState(() {});
  }

  removeToken() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.remove("token");
    setState(() {
      token = '';
    });
    prefs.clear();
  }

  bool get hasValidCookie => token != null;

  @override
  Widget build(BuildContext context) {
    if (future == null) {
      future = readToken();
      return LoaderIntro();
    }

    if (widget.cookie == 'logout' && nToken == 0) {
      removeToken();
      nToken = 1;
    }

    return Scaffold(
        body: Container(
            child: !hasValidCookie
                ? LoginScreen(onLoggedIn: (cookie) async {
                    var prefs = await SharedPreferences.getInstance();
                    prefs.setString('token', cookie);
                    setState(() {
                      token = cookie;
                    });
                  })
                : HomeCenters(
                    token: token, urlName: 'centros?webview_token=' + token)));
  }
}
