import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

const _url =
    'https://monkeyfit-api-prod.herokuapp.com/api/v1.1/accounts/login/';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key, this.onLoggedIn}) : super(key: key);
  final void Function(String cookie) onLoggedIn;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> _key = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  RegExp emailRegExp =
      new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  RegExp contRegExp = new RegExp(r'^([1-zA-Z0-1@.\s]{1,255})$');
  String _correo;
  String _contrasena;
  bool loaderLogin = true;
  int closeSeccion = 0;
  bool showPassword = true;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return new Future(() => false);
      },
      child: Scaffold(key: _scaffoldKey, body: loginForm()),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void _handleSubmitted() async {
    final FormState form = _key.currentState;
    if (!form.validate()) {
      return;
    } else {
      if (!_key.currentState.validate()) {
        showInSnackBar('Ingresa correctamente tus datos.');
        return;
      }

      if (_key.currentState.validate()) {
        _key.currentState.save();

        setState(() {
          loaderLogin = false;
        });

        final response = await http.post(_url, body: {
          'email': _correo,
          'password': _contrasena,
          'type': 'password'
        });

        var jsonResponse = convert.jsonDecode(response.body);

        try {
          var token = jsonResponse['token'];
          if (response.statusCode == 200) {
            if (closeSeccion == 0) {
              widget.onLoggedIn(token);
              setState(() {
                closeSeccion = 1;
                loaderLogin = true;
              });
            } else {
              showInSnackBar('Cierra la aplicacion y vuelve iniciar 🙏');
              setState(() {
                loaderLogin = true;
              });
            }
          } else {
            setState(() {
              loaderLogin = true;
            });
            showInSnackBar('Un dato es incorrecto.');
          }
        } catch (e) {
          showInSnackBar(
              'Intenta saliendo y volviendo a iniciar la aplicacion');
        }
      }
    }
  }

  Widget loginForm() {
    return Container(
      child: ListView(
        children: <Widget>[
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 80.0, bottom: 20.0),
              child: Image.asset('assets/logo-azul.png',
                  fit: BoxFit.cover, alignment: Alignment.center, width: 250),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.0),
            padding: EdgeInsets.only(left: 30.0, right: 30.0),
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    validator: (text) {
                      if (text.length == 0 || text == null) {
                        return "El correo es requerido";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelStyle: TextStyle(
                        fontSize: 15,
                        fontFamily: 'GTCinetypeRegular',
                        color: Color.fromRGBO(82, 82, 83, 1.0),
                      ),
                      labelText: 'Correo electrónico',
                      counterText: '',
                      icon: Icon(Icons.email,
                          size: 32.0, color: Color.fromRGBO(6, 21, 255, 1.0)),
                    ),
                    onSaved: (text) => _correo = text,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                        flex: 8,
                        child: TextFormField(
                          onSaved: (text) => _contrasena = text,
                          // obscureText: true,
                          obscureText: showPassword,
                          validator: (text) {
                            if (text.length == 0 || text == null) {
                              return "La contraseña es requerida";
                            }
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          maxLength: 20,
                          decoration: InputDecoration(
                            labelText: 'Contraseña',
                            labelStyle: TextStyle(
                              fontSize: 15,
                              fontFamily: 'GTCinetypeRegular',
                              color: Color.fromRGBO(82, 82, 83, 1.0),
                            ),
                            counterText: '',
                            icon: Icon(
                              Icons.lock,
                              size: 32.0,
                              color: Color.fromRGBO(6, 21, 255, 1.0),
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: IconButton(
                            color: Colors.white,
                            icon: Icon(
                              showPassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              size: 20.0,
                              color: Color.fromRGBO(82, 82, 83, 1.0),
                            ),
                            onPressed: () {
                              if (showPassword) {
                                setState(() {
                                  showPassword = false;
                                });
                              } else {
                                setState(() {
                                  showPassword = true;
                                });
                              }
                            }),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 30.0),
            margin: EdgeInsets.only(top: 25.0, bottom: 25.0),
            child: InkWell(
                onTap: () => launch(
                    'https://www.monkeyfitpass.com/recuperar-contrasena'),
                child: Text('¿Has olvidado tu contraseña?',
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'GTCinetypeRegular',
                      color: Color.fromRGBO(6, 21, 255, 1.0),
                    ))),
          ),
          Center(
            child: loaderLogin
                ? RaisedButton(
                    padding: EdgeInsets.only(
                        top: 12.0, bottom: 12.0, left: 20.0, right: 20.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    elevation: 5.0,
                    onPressed: _handleSubmitted,
                    color: Color.fromRGBO(6, 21, 255, 1.0),
                    child: Text('Iniciar sesión',
                        style: TextStyle(
                            fontSize: 18,
                            fontFamily: 'stolzl_displaybold',
                            color: Colors.white)),
                  )
                : CircularProgressIndicator(
                    strokeWidth: 5.0,
                    backgroundColor: Color.fromRGBO(6, 21, 255, 1.0),
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
          ),
          Center(
              child: Container(
            margin: EdgeInsets.only(top: 30.0),
            child: InkWell(
                onTap: _launchURL,
                child: Text('Regístrate',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontFamily: 'stolzl_displaybold',
                      color: Color.fromRGBO(6, 21, 255, 1.0),
                    ))),
          ))
        ],
      ),
    );
  }
}

_launchURL() async {
  await launch('https://www.monkeyfitpass.com/crear-cuenta-corporativa');
}
