import 'package:flutter/material.dart'
    show
        Alignment,
        BuildContext,
        Colors,
        Column,
        Flexible,
        FractionallySizedBox,
        Image,
        Material,
        SafeArea,
        SizedBox,
        State,
        StatefulWidget,
        Widget;

class LoaderIntro extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<LoaderIntro> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          SizedBox(height: 200.0),
          Flexible(
            // flex: 3,
            child: SafeArea(
              child: FractionallySizedBox(
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/logo-azul.png',
                  height: 250.0,
                  width: 250.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
