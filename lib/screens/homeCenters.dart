import 'package:Monkeyfitpass/screens/homeCuenta.dart' show MiCuenta;
import 'package:Monkeyfitpass/screens/newViewCenter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';

class HomeCenters extends StatefulWidget {
  const HomeCenters({Key key, this.token, this.urlName}) : super(key: key);
  final String token;
  final String urlName;

  @override
  _MonkeyAppState createState() => _MonkeyAppState();
}

class _MonkeyAppState extends State<HomeCenters> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: myAppBar(),
      body: Container(
          margin: EdgeInsets.only(top: 30.0),
          child: ListView(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Color.fromRGBO(6, 21, 255, 1.0)
                              .withOpacity(.5)),
                      borderRadius: BorderRadius.circular(6.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2.0,
                          color:
                              Color.fromRGBO(6, 21, 255, 1.0).withOpacity(.5),
                          offset: Offset(1.0, 3.5),
                        ),
                      ],
                      color: Colors.white,
                    ),
                    width: 150.0,
                    height: 150.0,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(PageTransition(
                            type: PageTransitionType.slideInLeft,
                            child: MiCuenta(token: widget.token)));
                      },
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: Image.asset('assets/perfil.png',
                                  fit: BoxFit.cover,
                                  alignment: Alignment.center,
                                  width: 120),
                            ),
                          ],
                        ),
                      ),
                    )
                  ),
                  // cardSecction('perfil.png', '${widget.token}', 'perfil/tu-perfil'),
                  cardSecction('live.png', '${widget.token}', 'planner')
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  cardSecction('beneficios.png', '${widget.token}', 'beneficios'),
                ],
              )
            ],
          )),
    );
  }

  Widget myAppBar() {
    return AppBar(
      title: Container(
          margin: EdgeInsets.only(top: 5.0),
          child: Image.asset(
            'assets/logo_blanco.png',
            width: 200.0,
          )),
      centerTitle: true,
    );
  }

  Widget cardSecction(icono, token, ruta) {
    return Container(
        margin: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          border: Border.all(
              color: Color.fromRGBO(6, 21, 255, 1.0).withOpacity(.5)),
          borderRadius: BorderRadius.circular(6.0),
          boxShadow: [
            BoxShadow(
              blurRadius: 2.0,
              color: Color.fromRGBO(6, 21, 255, 1.0).withOpacity(.5),
              offset: Offset(1.0, 3.5),
            ),
          ],
          color: Colors.white,
        ),
        width: 150.0,
        height: 150.0,
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(PageTransition(
                type: PageTransitionType.slideInLeft,
                child: NewViewCenter(token: token, urlName: ruta)));
          },
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Image.asset('assets/$icono',
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                      width: 120),
                ),
              ],
            ),
          ),
        ));
  }
}
