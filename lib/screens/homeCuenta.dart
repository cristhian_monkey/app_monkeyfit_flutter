import 'dart:convert' as convert;
import 'package:Monkeyfitpass/screens/homeScreen.dart';
import 'package:Monkeyfitpass/screens/loginScreen.dart';
import 'package:Monkeyfitpass/screens/info_profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

const _ruta = 'https://monkeyfit-api-prod.herokuapp.com/api/v1.1/';

class MiCuenta extends StatefulWidget {
  const MiCuenta({Key key, this.token}) : super(key: key);
  final String token;

  @override
  _MyDataState createState() => _MyDataState();
}

class DataMembresia {
  String name;
  String price;
  String dueDate;

  DataMembresia({this.name, this.price, this.dueDate});
}

class MyData {
  String name;
  String photo;
  String peso;
  String estatura;
  String objetivo;
  String genero;
  String lastname;
  String email;
  String phone;
  String address;
  String physicalCurrentState;

  MyData(
      {this.name,
      this.photo,
      this.peso,
      this.estatura,
      this.objetivo,
      this.genero,
      this.lastname,
      this.email,
      this.phone,
      this.physicalCurrentState,
      this.address});
}

var theData = MyData();
var dataMembresia = DataMembresia();
String myPhoto;

Future<Map<dynamic, dynamic>> fetchPostData(token) async {
  var tokenD = token;

  http.Response responseData = await http.get(
      Uri.encodeFull(_ruta + 'accounts/user/'),
      headers: {"Authorization": 'Token ' + tokenD});
  var jsonResponseData = convert.jsonDecode(responseData.body);

  http.Response responseMembresia = await http.get(
      Uri.encodeFull(_ruta + 'accounts/planner/'),
      headers: {"Authorization": 'Token ' + tokenD});
  var jsonMembresia = convert.jsonDecode(responseMembresia.body);
  print('jsonMembresia $jsonMembresia');

  if (jsonMembresia['can_renew'] != null) {
    dataMembresia.name = jsonMembresia['plan']['slug'];
    dataMembresia.dueDate = jsonMembresia['renewal_date'].toString();
    dataMembresia.price =
        jsonMembresia['plan']['plan_details']['sale_price'].toString();
  } else {
    dataMembresia.name = 'Aún no tienes un plan';
    dataMembresia.dueDate = '...';
    dataMembresia.price = '0.0';
  }

  if (responseData.statusCode == 200) {
    Map<dynamic, dynamic> midata = jsonResponseData;
    theData.name = jsonResponseData['first_name'];
    theData.lastname = jsonResponseData['last_name'];
    theData.email = jsonResponseData['email'];
    theData.phone = jsonResponseData['phone'].toString();

    if (jsonResponseData['profile_picture'] != null) {
      myPhoto =
          'https://res.cloudinary.com/hzkmhssql/image/upload/w_100,c_scale/remote_media/media/' +
              jsonResponseData['profile_picture'];
    } else {
      if (jsonResponseData['profile'].length > 0) {
        if (jsonResponseData['profile']['gender'] == 'M') {
          myPhoto =
              'https://monkeyfit-api-prod.s3.amazonaws.com/media/amenity/placeholder-profile-sq.jpg';
        } else {
          myPhoto =
              'https://monkeyfit-api-prod.s3.amazonaws.com/media/amenity/placeholder-profile-mujer.png';
        }
      } else {
        myPhoto =
            'https://monkeyfit-api-prod.s3.amazonaws.com/media/amenity/placeholder-profile-sq.jpg';
      }
    }

    if (jsonResponseData['profile'].length > 0) {

      if (jsonResponseData['profile']['goal'] == 'lose-weight') {
        theData.objetivo = 'Bajar de peso';
      } 
      else if (jsonResponseData['profile']['goal'] == 'build-muscle') {
        theData.objetivo = 'Ganar masa muscular';
      } 
      else if (jsonResponseData['profile']['goal'] == 'get-in-shape') {
        theData.objetivo = 'Mantenerme saludable';
      } 
      else {
        theData.objetivo = 'Tonificar';
      }

      if (jsonResponseData['profile']['physical_current_state'] == 'slightly-active') {
        theData.physicalCurrentState = 'Ligeramente activo';
      } 
      else if (jsonResponseData['profile']['physical_current_state'] == 'moderately-active') {
        theData.physicalCurrentState = 'Moderadamente activo';
      } 
      else {
        theData.physicalCurrentState = 'Bastante activo';
      }

      theData.estatura = jsonResponseData['profile']['height'].toString();
      theData.genero = jsonResponseData['profile']['gender'] == 'M'
          ? 'Masculino'
          : 'Femenino';
      theData.peso = jsonResponseData['profile']['weight'].toString() + ' Kg';
    } else {
      theData.objetivo = '...';
      theData.estatura = '...';
      theData.genero = '...';
      theData.peso = '...';
    }

    if (jsonResponseData['address'] == null) {
      theData.address = '...';
    } else {
      theData.address = jsonResponseData['address'];
    }
    return midata;
  } else {
    throw Exception('Failed to load post');
  }
}

class _MyDataState extends State<MiCuenta> {
  Future<Map<dynamic, dynamic>> _theData;

  @override
  void initState() {
    super.initState();
    _theData = fetchPostData(widget.token);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _theData,
        builder: (BuildContext context, AsyncSnapshot<dynamic> dataUser) {
          if (dataUser.hasData) {
            return Scaffold(
                appBar: AppBar(
                  backgroundColor: Color.fromRGBO(6, 21, 255, 1.0),
                  title: Text('Mi Perfil',
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'stolzl_displaybold')),
                ),
                body: Stack(children: <Widget>[
                  ListView(children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 100),
                          child: Container(child: Text(''),),),
                        Container(
                          margin: EdgeInsets.only(right: 10.0),
                          width: 70.0,
                          height: 70.0,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 2.0,
                                color: Color.fromRGBO(0, 222, 217, 1.0),
                              ),
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage("$myPhoto"))),
                        ),
                        Text(
                          '${dataUser.data["first_name"]}',
                          style: TextStyle(
                            color: Color.fromRGBO(82, 82, 83, 1.0),
                            fontWeight: FontWeight.bold,
                            fontFamily: 'stolzl_displaybold',
                            fontSize: 19.0,
                          ),
                        )
                      ]
                    ),
                    Container(
                        height: 220.0,
                        margin:
                            EdgeInsets.only(right: 15.0, left: 15.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 7.0,
                              color: Color.fromRGBO(82, 82, 83, 1.0)
                                  .withOpacity(.5),
                              offset: Offset(3.0, 5.0),
                            ),
                          ],
                          color: Colors.white,
                        ),
                        padding: EdgeInsets.only(
                          top: 10,
                          left: 15,
                          right: 0,
                          bottom: 10
                        ),
                        child: ListView(
                          children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Mis Datos :',
                                style: TextStyle(
                                  color:
                                      Color.fromRGBO(82, 82, 83, 1.0),
                                  fontFamily: 'stolzl_displaybold',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                )
                              ),
                              RaisedButton(
                                elevation: 0.0,
                                onPressed: launchUrlEditData,
                                color: Colors.white,
                                child: Icon(Icons.edit,
                                    color: Color.fromRGBO(6, 21, 255, 1.0)
                                        .withOpacity(0.8),
                                    size: 30.0),
                              ),
                            ]
                          ),
                          // infoProfile(),
                          infoProfile('Apellido: ','${theData.lastname}'),
                          infoProfile('Celular: ','${theData.phone}'),
                          infoProfile('Género: ','${theData.genero}'),
                          infoProfile('Objetivo: ','${theData.objetivo}'),
                          infoProfile('Actividad: ','${theData.physicalCurrentState}'),
                          infoProfile('Peso: ','${theData.peso}'),
                          infoProfile('Estatura: ','${theData.estatura}'),
                          infoProfile('Dirección: ','${theData.address}'),
                        ]
                      )
                    ),
                    Container(
                        height:
                            '${dataMembresia.dueDate}' == '...' ? 160.0 : 140.0,
                        margin:
                            EdgeInsets.only(top: 10.0, right: 15.0, left: 15.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 7.0,
                              color: Color.fromRGBO(82, 82, 83, 1.0)
                                  .withOpacity(.5),
                              offset: Offset(3.0, 5.0),
                            ),
                          ],
                          color: Colors.white,
                        ),
                        child: Stack(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(top: 20.0),
                              padding: EdgeInsets.only(left: 10.0),
                              child: Row(children: <Widget>[
                                Container(
                                    child: '${dataMembresia.dueDate}' != '...'
                                        ? Text('Membresía Actual',
                                            style: TextStyle(
                                              color: Color.fromRGBO(
                                                  82, 82, 83, 1.0),
                                              fontFamily: 'stolzl_displaybold',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20.0,
                                            ))
                                        : Text('Membresía',
                                            style: TextStyle(
                                              color: Color.fromRGBO(
                                                  82, 82, 83, 1.0),
                                              fontFamily: 'stolzl_displaybold',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20.0,
                                            )))
                              ])),
                          Container(
                              margin: EdgeInsets.only(top: 50.0),
                              padding: EdgeInsets.only(left: 20.0),
                              child: Row(children: <Widget>[
                                Container(
                                    child: Text('Plan:',
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(6, 21, 255, 1.0),
                                          fontFamily: 'stolzl_displaybold',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0,
                                        ))),
                                Container(
                                    padding: EdgeInsets.only(left: 5.0),
                                    child: Text(
                                        '${dataMembresia.name.toUpperCase()}',
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(82, 82, 83, 1.0),
                                          fontFamily: 'GTCinetypeRegular',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0,
                                        ))),
                              ])),
                          Container(
                              margin: EdgeInsets.only(top: 75.0),
                              padding: EdgeInsets.only(left: 20.0),
                              child: Row(children: <Widget>[
                                Container(
                                    child: Text('Termina:',
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(6, 21, 255, 1.0),
                                          fontFamily: 'stolzl_displaybold',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0,
                                        ))),
                                Container(
                                    padding: EdgeInsets.only(left: 5.0),
                                    child: Text('${dataMembresia.dueDate}',
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(82, 82, 83, 1.0),
                                          fontFamily: 'GTCinetypeRegular',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0,
                                        ))),
                              ])),
                          Container(
                              margin: EdgeInsets.only(top: 100.0),
                              padding: EdgeInsets.only(left: 20.0),
                              child: Row(children: <Widget>[
                                Container(
                                    child: Text('Precio:',
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(6, 21, 255, 1.0),
                                          fontFamily: 'stolzl_displaybold',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0,
                                        ))),
                                Container(
                                    padding: EdgeInsets.only(left: 5.0),
                                    child: Text('S/. ${dataMembresia.price}',
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(82, 82, 83, 1.0),
                                          fontFamily: 'GTCinetypeRegular',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0,
                                        ))),
                              ])),
                          Container(
                              margin: EdgeInsets.only(top: 130.0),
                              padding: EdgeInsets.only(left: 20.0),
                              child: '${dataMembresia.dueDate}' == '...'
                                  ? InkWell(
                                      onTap: () => launch(
                                          'https://www.monkeyfitpass.com/planes-corporativos'),
                                      child: Text('Ver planes',
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color:
                                                Color.fromRGBO(6, 21, 255, 1.0),
                                            fontFamily: 'stolzl_displaybold',
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.0,
                                          )))
                                  : Container())
                        ])),
                    launCardSecction(launchUrlReferidos, Icons.share, 'Regala y Gana'),
                    launCardSecction(launchUrlTarjetas, Icons.credit_card, 'Tarjetas'),
                    launCardSecction(launchUrlMisPagos, Icons.turned_in_not, 'Historial de pagos'),
                    launCardSecction(launchUrlFamiliar, Icons.home, 'Plan Familiar'),
                    Container(
                      height: 60.0,
                      margin: EdgeInsets.only(
                          top: 10.0, right: 15.0, left: 20.0, bottom: 10.0),
                      child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      HomeScreen(cookie: 'logout')),
                            );
                            Navigator.of(context).push(PageTransition(
                                type: PageTransitionType.slideUp,
                                child: LoginScreen(onLoggedIn: (cookie) {})));
                          },
                          child: Row(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(right: 7.0),
                                child: Icon(Icons.input,
                                    color: Color.fromRGBO(6, 21, 255, 1.0)
                                        .withOpacity(.5)),
                              ),
                              Text('Cerrar sesión',
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      color: Color.fromRGBO(6, 21, 255, 1.0),
                                      fontFamily: 'stolzl_displaybold'))
                            ],
                          )),
                    ),
                  ]),
                ]));
          } else {
            return Scaffold(
                appBar: AppBar(
                  backgroundColor: Color.fromRGBO(6, 21, 255, 1.0),
                  title: Text('Mi Perfil',
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'stolzl_displaybold')),
                ),
                body: Container(
                  child: Center(
                      child: Text('Cargando...',
                          style: TextStyle(
                              color: Color.fromRGBO(6, 21, 255, 1.0),
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold))),
                ));
          }
        });
  }
}


Widget launCardSecction(method, iconData, nameText) {
  return Container(
    height: 40.0,
    margin:
        EdgeInsets.only(top: 10.0, right: 15.0, left: 15.0),
    child: RaisedButton(
        onPressed: method,
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Icon(
              iconData,
              color: Color.fromRGBO(6, 21, 255, 1.0),
              size: 22.0
            ),
            Padding(
              padding: EdgeInsets.only(left: 5 ),
              child: Text(
                nameText,
                style: 
                  TextStyle(
                    fontSize: 17,
                    color: Color.fromRGBO(82, 82, 83, 1.0),
                    fontFamily: 'stolzl_displaybold')
                  ),
            )
          ],
        )),
  );
}


launchUrlBeneficios() async {
  await launch('https://www.monkeyfitpass.com/beneficios');
}

launchUrlFamiliar() async {
  await launch('https://www.monkeyfitpass.com/perfil/mi-plan-corporativo');
}

launchUrlMisPagos() async {
  await launch('https://www.monkeyfitpass.com/perfil/historial-de-pagos');
}

launchUrlTarjetas() async {
  await launch('https://www.monkeyfitpass.com/perfil/mis-tarjetas');
}
launchUrlReferidos() async {
  await launch('https://www.monkeyfitpass.com/perfil/mi-plan-corporativo');
}

launchUrlEditData() async {
  await launch('https://www.monkeyfitpass.com/perfil/mi-perfil');
}