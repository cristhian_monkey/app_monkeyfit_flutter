import 'package:Monkeyfitpass/screens/LoginScreen.dart';
import 'package:Monkeyfitpass/screens/homeCenters.dart';
import 'package:Monkeyfitpass/screens/homeCuenta.dart';
import 'package:Monkeyfitpass/screens/homeScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';

var currentLocation;
String error;

void myLocation() async {
  var location = new Location();
  try {
    currentLocation = await location.getLocation();
  } on PlatformException catch (e) {
    if (e.code == 'PERMISSION_DENIED') {
      error = 'Permission denied';
    }
    currentLocation = null;
  }
}

class AppBasica extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    myLocation();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      // DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Color.fromRGBO(6, 21, 255, 1.0),
        ),
        home: HomeScreen(),
        routes: {
          '/login': (context) => LoginScreen(),
          '/centros': (context) => HomeCenters(),
          '/perfil': (context) => MiCuenta(),
        });
  }
}
